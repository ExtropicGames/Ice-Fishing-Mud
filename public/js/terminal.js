// TODO: convert me to coffeescript!

var println = function(text, player) {
  if (player) {
    $("<tr class='info'><td><tt>&gt; " + text + "</tt></td></tr>").insertBefore($('#last'));
  } else {
    $("<tr class='info'><td><tt>" + text + "</tt></td></tr>").insertBefore($('#last'));
  }
}

$(document).ready(function() {

  var terminal_input = $('#terminal-input');

  $('#last').addClass('hidden');
  $.ajax({
    url: 'do/welcome',
    data: { 'welcome': true },
    dataType: 'json',
    type: 'POST'
  }).done(function(res) {
    console.log(res);
    if (res.error) {
      println(res.error);
    } else {
      println(res.text);
    }
    $('#last').removeClass('hidden');
  });

  terminal_input.keyup(function(e) {
    if (e.keyCode == 13) {
      e.preventDefault();
      var command = terminal_input.val();
      terminal_input.val('');
      println(command, true);
      $('#last').addClass('hidden');
      $.ajax({url: 'do', data: { 'command': command }, dataType: 'json', type: 'POST'}).done(function(res) {
        if (res.error) {
          println(res.error)
        } else {
          println(res.text)
        }
        $('#last').removeClass('hidden');
      });
    }
  });
});
