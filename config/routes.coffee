# Draw routes.  Locomotive's router provides expressive syntax for drawing
# routes, including support for resourceful routes, namespaces, and nesting.
# MVC routes can be mapped mapped to controllers using convenient
# `controller#action` shorthand.  Standard middleware in the form of
# `function(req, res, next)` is also fully supported.  Consult the Locomotive
# Guide on [routing](http://locomotivejs.org/guide/routing.html) for additional
# information.
module.exports = () -> 
  this.root('pages#main')
  this.match('login', 'pages#login')
  this.post('login', 'pages#login')
  this.match('admin', 'pages#admin')
  
  # API calls
  this.post('do/welcome', 'do#welcome')
  this.post('do', 'do#main')
