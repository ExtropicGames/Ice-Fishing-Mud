express = require('express')

module.exports = () -> this.use(express.errorHandler())