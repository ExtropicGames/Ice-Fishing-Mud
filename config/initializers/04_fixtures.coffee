
# TODO: Right now, all initializers run in parallel. Fixures need to be run only after migrations.

module.exports = () ->
  this.pg.connect(process.env.DATABASE_URL, (err, client, done) ->
    if (!client)
      console.log('Could not connect to database at ' + process.env.DATABASE_URL)
      console.log(err)
      done()
      return

    console.log('Loading fixtures')
    # The WHERE NOT EXISTS syntax is Postgres's equivalent of INSERT IGNORE.
    sql = "
      INSERT INTO rooms (room_id, description, detailed_description, name)
        SELECT
          1,
          'You are in the Frozen North.',
          'Your eyes hurt.',
          'A tundra'
        WHERE NOT EXISTS (SELECT 1 FROM rooms WHERE room_id=1)
    ";

    client.query(sql, (err, result) ->
      if (err)
        console.log('Error loading fixtures: ', err)
      else
        console.log('Fixtures loaded')

      done()
    )
  )
