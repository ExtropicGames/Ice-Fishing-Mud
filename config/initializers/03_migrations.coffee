newestVersion = 2;

migrate_up = (client, migrations, done) ->
  if (migrations.length == 0)
    done()
    return
  m_number = migrations.shift()
  console.log('Migrating to version ' + m_number)
  m_script = require('../migrations/' + m_number + '.coffee')
  m_script.up(client, (err) ->
    if (err)
      console.log('Error running migration %d: ', m_number, err)
      done()
      return

    client.query('UPDATE migration SET version = ' + m_number, (err, result) ->
      if (err)
        console.log(err)
        done()
        return
      migrate_up(client, migrations, () -> done())
    )
  )

migrate_down = (client, migrations, done) ->
  if (migrations.length == 0)
    done()
    return
  m_number = migrations.shift()
  console.log('Migrating to version ' + m_number-1)
  m_script = require('../migrations/' + m_number + '.coffee')
  m_script.down(client, (err) ->
    if (err)
      console.log('Error running migration %d: ', m_number, err)
      done()
      return

    client.query('UPDATE migration SET version = ' + (m_number-1), (err, result) ->
      if (err)
        console.log(err)
        done()
        return
      migrate_down(client, migrations, () -> done())
    )
  )

run_migrations = (client, currentVersion, done) ->
  console.log('migration version is at ' + currentVersion)
  if (currentVersion < newestVersion)
    migrate_up(client, [currentVersion+1..newestVersion], done)
    return
  else if (currentVersion > newestVersion)
    migrate_down(client, [currentVersion..newestVersion+1], done)
    return
  console.log('No migrations needed')
  done()

module.exports = () ->
  context = this
  context.pg.connect(process.env.DATABASE_URL, (err, client, done) ->

    if (!client)
      console.log('Could not connect to database at ' + process.env.DATABASE_URL)
      console.log(err)
      done()
      return

    console.log('Connected to database at ' + client.host)
    client.query('SELECT * FROM migration', (err, result) ->

      # update migration table
      if (err)
        sql = '
          CREATE TABLE migration (version integer NOT NULL);
          INSERT INTO migration (version) VALUES (0);
        '
        client.query(sql, (err, result) ->
          if (err)
            console.log('Error updating migration table ' + err)
            return
          run_migrations(client, 0, () -> done())
        )
      else
        if (result.rowCount == 0)
          client.query('INSERT INTO migration (version) VALUES (0)', (err, result) ->
            if (err)
              console.log('Error inserting into migration table ' + err)
              return
            run_migrations(client, 0, () -> done())
          )
        else
          run_migrations(client, result.rows[0].version, () -> done())
    )
  )
