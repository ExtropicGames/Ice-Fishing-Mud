exports.up = (client, done) ->
  # Add flags to people: admin, anonymous
  # Add name to rooms table
  sql = '
    ALTER TABLE people     ADD flags integer;
    ALTER TABLE rooms      ADD name  varchar;
    ALTER TABLE room_links ADD travel_description varchar;
  '
  client.query(sql, (err) ->
    console.log(err) if (err)
    done()
  )

exports.down = (client, done) ->
  sql = '
    ALTER TABLE people     DROP IF EXISTS flags;
    ALTER TABLE rooms      DROP IF EXISTS name;
    ALTER TABLE room_links DROP IF EXISTS travel_description;
  '
  client.query(sql, (err) ->
    console.log(err) if (err)
    done()
  )
