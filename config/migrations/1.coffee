exports.up = (client, done) ->
  sql = '
    CREATE TABLE rooms (
      room_id               SERIAL PRIMARY KEY,
      description           varchar,
      detailed_description  varchar
    );
    CREATE TABLE people (
      person_id             SERIAL PRIMARY KEY,
      name                  varchar,
      username              varchar UNIQUE,
      password              varchar,
      room_id               integer references rooms(room_id),
      joined_on             timestamp
    );
    CREATE TABLE tchotchkes (
      tchotchke_id          SERIAL PRIMARY KEY,
      name                  varchar,
      flags                 integer,
      description           varchar,
      detailed_description  varchar
    );
    CREATE TABLE room_links (
      go_from               integer references rooms(room_id),
      come_to               integer references rooms(room_id),
      direction             integer
    );
    CREATE TABLE inventories (
      person_id             integer references people(person_id),
      tchotchke_id          integer references tchotchkes(tchotchke_id),
      count                 integer
    );
    CREATE TABLE piles (
      room_id               integer references rooms(room_id),
      tchotchke_id          integer references tchotchkes(tchotchke_id),
      count                 integer
    );
  '

  client.query(sql, (err) ->
    if (err) then console.log(err)
    done()
  )

exports.down = (client, done) ->
  sql = '
    DROP TABLE IF EXISTS room_links;
    DROP TABLE IF EXISTS inventories;
    DROP TABLE IF EXISTS piles;
    DROP TABLE IF EXISTS people;
    DROP TABLE IF EXISTS rooms;
    DROP TABLE IF EXISTS tchotchkes;
  '
  client.query(sql, (err) ->
    if (err) then console.log(err)
    done()
  )
