Live at http://ice-fishing-adventures.herokuapp.com/.

License: WTFPL

# Design / Brainstorming

Primary features of the game:

- Text console where you can type in commands
- Parser that interprets and responds to the commands
- Fishing simulator
- Canadians

Possible names:

- Ice Fishing Adventures
- IFFMUD (Icelandic Ice Fishing MUD)

Example game session:

```
> north
I don’t know how to ‘north’.
> go north
You are already in The Frozen North!
> go south
Better stay away from there. I hear the South is full of rednecks and worse, 
software developers.
> go east
I don’t see any ‘east’ here.
> fish
You lower your line into the ice hole.
> wait
You wait.
> wait
You wait.
> wait
You wait.
> wait
You wait.
> wait
You wait.
> wait
You wait.
> wait
You wait.
You feel a nibble on your line.
> reel in line
You reel in the line. You hooked a salmon! Hurray!
> score
You have scored 10 out of a possible 10,000,000,000,000 points. Keep on fishing!
> look
You see: A white bear, A white bear, and three white bears.
> punch bear
Which bear?
> white bear
I don’t know how to ‘white’
> punch white bear
I think you mean “punch THE white bear.” Proper grammar will get you far in life.
> punch the white bear
You punch the white bear. (Achievement unlocked: Bear Wrestler)
> look
You see: A white bear, A white bear, A injured white bear, and two white bears.
> use cellphone
You pull out your cellphone and log into FishNets, the world’s best BBS for ice 
fishing aficionados.

A menu screen greets you.

*----------------------------*
|                            |
| Welcome to FishNets!       |
| Users Online: 14           |
| (P)ost a message           |
| (D)ownload new messages    |
| (S)earch archived messages |
| (L)og off                  |
|                            |
*----------------------------*

> look
You’re busy playing with your cellphone and can’t do that right now.
> d

*** Message 1 of 49

From: CoolFishLover
Subj: Anyone here?
Date: Tue Apr 19 3:45 PM
Body:

Hey, I’ve been wandering through this frozen tundra for weeks now! Is there 
anyone else out here? I’m so alone...

Keep reading? (Y/N)

> y

*** Message 2 of 49

From: CaptnHook
Subj: Bears
Date: Tue Apr 20 11:32 AM
Body:

Look out for bears. One accosted me the other day and stole my fish. If anyone 
sees a bear with a my salmon, please return it to me at the shack in the frozen 
tundra next to the ice hole.

Keep reading? (Y/N)

> y

*** Message 3 of 49

From: CoolFishLover
Subj: RE: Bears
Date: Tue Apr 20 11:45 AM
Body:

You’ll have to be more specific. I must have passed a million shacks next to ice 
holes.

Keep reading? (Y/N)

> n

*----------------------------*
|                            |
| Welcome to FishNets!       |
| Users Online: 14           |
| (P)ost a message           |
| (D)ownload new messages    |
| (S)earch archived messages |
| (L)og off                  |
|                            |
*----------------------------*

> L

You switch off your phone. Back to ice fishing.

>

```

Future extensions:

- A social networking bulletin board system (BBS) network known as FishNets. 
  Users can chat about their favorite fishing techniques and see what their 
  friends are up to! Could also support Orkut and Pinterest integration in the 
  future.
- An AI assistant the user can chat with during gameplay. The AI would be called 
  A.L.ICE (Artificial Life and ICE fishing helper) and would provide useful 
  fishing tips a la Clippy from Microsoft Word.
- Achievements.
- Automated theorem prover as part of the parsing engine. Type in any proof and 
  it will automatically return an answer! Unfortunately, the answer is always 
  ‘nil’ as the feature is still in beta.
- ??? dinosaurs ???

Notes

- Polar bears are called white bears.
- Procedural music generation (players can play instruments)
- Travel to exotic frozen wastelands around the world: Canada, Iceland, Norway, 
  Siberia, Alaska, and Antartica
- “You see a wolf, a wolf, a wolf, a wolf, and three wolves.”
- Wildlife with a full predator/prey simulation and genetics system.
- Detailed weather simulator (but it is always either snowing or not snowing)

Parser

- go north
- get the pickaxe
- reel in the fishing line and take the fish
- take the apple and the cherry

# Development

### Install

```
brew install postgresql
ln -sfv /usr/local/opt/postgresql/*.plist ~/Library/LaunchAgents
launchctl load ~/Library/LaunchAgents/homebrew.mxcl.postgresql.plist
gem install heroku
heroku login
git clone git@github.com:extropic-engine/ice-fishing-mud.git
cd ice-fishing-mud
npm install
./local-db-setup
```

### Run

`foreman start`

### Deploy

`git push heroku master`

### Useful commands

* ```heroku ps``` - Show worker processes
* ```heroku open``` - Open app in browser
* ```heroku logs``` - View logs
