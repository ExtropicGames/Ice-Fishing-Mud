locomotive = require('locomotive')
Controller = locomotive.Controller

DoController = new Controller()

DoController.welcome = () ->
  context = this

  if (!context.req.session.person_id)
    context.pg = require('pg')
    context.pg.connect(process.env.DATABASE_URL, (err, client, done) ->
      if err
        console.log(err)
        done()
        return
      context.models = {
        people: require('../models/people'),
      }
      context.models.people.add_anonymous_user(client, (person) ->
        done()
        if person
          console.log('saving person id ' + person.person_id + ' to the session')
          context.req.session.person_id = person.person_id
          context.req.session.person_name = person.username
          # TODO: replace \n with <br> in html contexts
          context.res.json({'text': 'Welcome to the Frozen North, ' + person.username + '!\nTo the west you see an igloo.\nThere is a hole here.\n'})
        else
          console.log(person)
          context.res.json({'error': 'unknown error'})
      )
    )
  else
    this.res.json({'text': 'Welcome back, ' + this.req.session.person_name + '.\nYou are in the Frozen North.\nTo the west you see an igloo.\nThere is a hole here.\n'})

DoController.main = () ->
  context = this

  if (!context.req.session.person_id)
    console.log('Not authenticated')
    return context.res.json({'error': 'Not authenticated'})

  command = context.param('command')

  context.pg = require('pg')
  context.pg.connect(process.env.DATABASE_URL, (err, client, done) ->
    if err
      console.log(err)
      done()
      return context.res.json({'error': err})

    context.models = {}
    context.models.people = require('../models/people')
    context.models.rooms = require('../models/rooms')
    context.models.tchotchkes = require('../models/tchotchkes')
    context.models.people.get_person(client, context.req.session.person_id, (person) ->
      if (!person)
        done()
        return context.res.json({'error': 'Unknown session error'})
      context.models.rooms.get_room(client, person.room_id, (room) ->
        world = {}
        world.room = room
        world.person = person
        parse(context, client, command, world, (result) ->
          done()
          context.res.json({'text': result})
        )
      )
    )
  )

# This function takes an input string, parses it,
# then calls the appropriate verb function and mutates
# the world state appropriately. Afterwards, it calls the
# result callback with the string to be printed to the user.
parse = (context, client, command, world, callback) ->
  tokens = command.split /\s+/
  # TODO: a more intelligent parser would not assume that the verb is always the first word
  verb = tokens.shift().trim()
  verbs = require('../libs/parse/verbs')
  if verbs[verb]
    verbs[verb](context, client, tokens, world, (result) ->
      callback(result)
    )
  else
    callback("I don't know how to " + verb + ".");

module.exports = DoController
