locomotive = require('locomotive')
Controller = locomotive.Controller

PagesController = new Controller()

PagesController.main = () ->
  this.title = 'Ice Fishing Adventures'
  this.render()

PagesController.login = () ->
  this.pg = require('pg')
  self = this
  sess = this.req.session
  this.title = 'Ice Fishing Adventures'
  people = require('../models/people')
  this.login_type = (this.param('login_type'))
  if (this.login_type == 'new')
    # form validation
    if (!this.param('new_username'))
      this.err = 'Please enter a username.'
    if (!this.param('new_password') || !this.param('new_password_confirm'))
      this.err = 'Please enter a password.'
    if (this.param('new_password') != this.param('new_password_confirm'))
      this.err = 'Passwords do not match.'

    if (this.err)
      return this.render()

    # try to create account
    this.pg.connect(process.env.DATABASE_URL, (err, client) ->
      people.add_user_if_not_exists(client, self.param('new_username'), self.param('new_password'), (user_id) ->
        if (user_id)
          sess.user_id = user_id
          console.log(self.param('new_username') + ' created a new account')
          self.redirect('/')
        else
          self.error = 'Username is already in use.'
          self.render()
      )
    )

  else if (this.login_type == 'returning')
    # form validation
    if (!this.param('returning_username'))
      this.err = 'Please enter a username.'
    if (!this.param('returning_password'))
      this.err = 'Please enter a password.'

    if (this.err)
      return this.render()

    this.pg.connect(process.env.DATABASE_URL, (err, client) ->
      people.authenticate_user(client, self.param('returning_username'), self.param('returning_password'), (user_id) ->
        if (user_id)
          sess.user_id = user_id
          console.log(self.param('returning_username') + ' just logged in')
          self.redirect('/')
        else
          self.error = 'Invalid username or password.'
          self.render()
      )
    )
  else
    this.render()

PagesController.admin = () ->
  this.redirect('/login') if (!this.req.session.user)
  # TODO: add additional authentication so only admins can access this page
  this.pg = require('pg')
  this.models = {}
  this.models.rooms = require('../models/rooms')
  this.models.tchotchkes = require('../models/tchotchkes')
  self = this
  this.pg.connect(process.env.DATABASE_URL, (err, client) ->
    console.log(err) if err
    self.client = client
    self.models.rooms.get_all(client, (rooms) ->
      self.models.tchotchkes.get_all(client, (tchotchkes) ->
        self.title = 'Admin Console'
        self.rooms = rooms
        self.tchotchkes = tchotchkes
        self.tchotchke_simulacra = []
        self.render()
      )
    )
  )


module.exports = PagesController
