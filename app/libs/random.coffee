nouns = [
  'birchbark',
  'clawhammer',
  'snowflake',
  'foxglove',
  'dashboard',
  'hovercraft',
  'waterfall',
  'earthquake',
  'passport',
  'miniskirt',
  'hourglass',
  'eggplant',
  'hummingbird',
  'butterfly',
  'railroad',
  'grasshopper',
]

exports.anon_name = () ->
  name = 'Anonymous'
  name += '-' + nouns[Math.floor(Math.random() * nouns.length)] until name.length > 32
  return name
