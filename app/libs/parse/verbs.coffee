exports.get = (context, client, tokens, world, callback) ->
  target = tokens.shift()
  if (!target)
    callback('Get what?')
    return
  context.models.tchotchkes.get_pile(client, world.room.room_id, (tchotchkes) ->
    target_found = false
    for tchotch in tchotchkes
      do (tchotch) ->
        if (!target_found && tchotch.name == target)
          target_found = true
          # take it if we can
          if (tchotch.flags == context.models.tchotchkes.flags.IMMOBILE)
            callback("You can't take the " + tchotch.name + ".")
          else
            context.models.tchotchkes.take(client, world.room.room_id, tchotch.tchotchke_id, world.person.person_id, (success) ->
              if (success)
                callback('You take the ' + tchotch.name + '.')
              else
                callback('The ' + tchotch.name + ' seems to have vanished!')
            )
    if (!target_found)
      context.models.tchotchkes.get_inventory(client, world.person.person_id, (tchotchkes) ->
        target_found = false
        for tchotch in tchotchkes
          do (tchotch) ->
            if (!target_found && tchotch.name == target)
              target_found = true
              callback("You're already holding the " + tchotch.name + "!")
        if (!target_found)
          callback("I don't see any " + target + " " + tokens.join(' ') + " here.")
      )
  )

exports.go = (context, client, tokens, world, done) ->
  dir = tokens.shift().toLowerCase()[0]
  context.models.rooms.get_link(client, world.person.room_id, dir, (room) ->
    if (room)
      console.log('moving to room')
      context.models.people.move_to(client, world.person.person_id, room.room_id, (success) ->
        if (success)
          return done(room.detailed_description)
        return done('i am stucke')
      )
    else
      not_a_room = {
        'n': 'You are already in the Frozen North!',
        's': "You really don't want to go there. Trust me.",
        'e': "Don't you want to check out this hole first?",
        'w': "You go west.",
      }
      if (not_a_room[dir])
        return done(not_a_room[dir])
      if (!dir)
        return done('Go where?')
      # TODO: map tokens into a properly formatted string
      return done("'" + dir + " " + tokens.join(' ') + "' is not a place.")
  )

exports.fish = (context, client, tokens, world, callback) ->
  callback 'You lower your line into the ice hole.'

exports.wait = (context, client, tokens, world, callback) ->
  callback 'You wait.'

exports.look = (context, client, tokens, world, callback) ->
  context.models.rooms.get_current_room(client, world.person.person_id, (room) ->
    callback(room.detailed_description)
  )
