exports.flags = {
  IMMOBILE: 1  # cannot be taken
}
exports.get_pile = (client, room_id, callback) ->
  q = client.query("SELECT t.* FROM tchotchkes t JOIN piles p ON (t.tchotchke_id = p.tchotchke_id) WHERE p.room_id=" + room_id)
  rows = []
  q.on('error', (err) ->
    console.log(err)
  )
  q.on('row', (row) ->
    rows.push(row)
  )
  q.on('end', (result) ->
    callback(rows)
  )
  
exports.get_inventory = (client, person_id, callback) ->
  q = client.query("SELECT t.* FROM tchotchkes t JOIN inventories i ON (t.tchotchke_id = i.tchotchke_id) WHERE i.person_id=" + person_id)
  rows = []
  q.on('error', (err) ->
    console.log(err)
  )
  q.on('row', (row) ->
    rows.push(row)
  )
  q.on('end', (result) ->
    callback(rows)
  )

exports.get_all = (client, callback) ->
  q = client.query('SELECT * FROM tchotchkes')
  rows = []
  q.on('error', (err) ->
    console.log(err)
  )
  q.on('row', (row) ->
    rows.push(row)
  )
  q.on('end', () ->
    callback(rows)
  )
  
exports.take = (client, room_id, tchotchke_id, person_id, quantity, callback) ->
  q = client.query("UPDATE piles SET count=count-1 WHERE room_id=" + room_id + " AND tchotchke_id=" + tchotchke_id + " AND count > 0 LIMIT 1")
  q.on('error', (err) ->
    console.log(err)
  )
  q.on('end', (result) ->
    # check row count to see if item was taken
    if (result.rowCount == 1)
      callback(true)
      q2 = client.query("UPDATE inventories SET count=count+1 WHERE person_id=" + person_id + " AND tchotchke_id=" + tchotchke_id + " LIMIT 1")
      q2.on('error', (err) ->
        console.log(err)
      )
      q2.on('end', (result) ->
        # check row count to see if inventory was updated
        if (result.rowCount == 0)
          q3 = client.query("INSERT INTO inventories (person_id, tchotchke_id, count) VALUES (" + person_id + ", " + tchotchke_id + ", 1)")
          q3.on('error', (err) ->
            console.log(err)
          )
      )
    else
      # item could not be taken, maybe someone else got it first
      callback(false)
  )