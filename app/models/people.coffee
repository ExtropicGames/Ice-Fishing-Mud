# Users are people too.

ERROR_SQL_DUPLICATE_KEY = 23505

STARTING_ROOM_ID = 1

# Returns true if the user was added, false if the user already existed.
# Callback is called when the query is done executing.
exports.add_user_if_not_exists = (client, username, password, callback) ->
  sql = "
    INSERT INTO people
      (username, password, joined_on, room_id)
    VALUES
      ('" + username + "', '" + password + "', NOW(), " + STARTING_ROOM_ID + ")
    RETURNING person_id
  "
  client.query(sql, (err, result) ->
    if err
      console.log(err) unless (err.code == ERROR_SQL_DUPLICATE_KEY)
      # TODO: still possible to send user id to callback in this case?
      callback()
      return
    if (result.rowCount != 1)
      callback()
      return
    callback(result.rows[0].person_id)
  )

exports.add_anonymous_user = (client, callback) ->
  random = require('../libs/random')
  name = random.anon_name()
  console.log('adding anonymous user ' + name)
  sql = "
    INSERT INTO people
      (joined_on, room_id, username)
    VALUES
      (NOW(), " + STARTING_ROOM_ID + ", '" + name + "')
    RETURNING person_id, username
  "
  console.log(sql)
  client.query(sql, (err, result) ->
    if err
      console.log(err)
      callback()
      return
    console.log(result)
    callback(result.rows[0])
  )

# Returns true if the user is authenticated.
exports.authenticate_user = (client, username, password, callback) ->
  client.query("SELECT * FROM people WHERE username='" + username + "' AND password='" + password + "'", (err, result) ->
    if err
      console.log(err)
      callback()
      return
    if (result.rowCount != 1)
      callback()
      return
    callback(result.rows[0].person_id)
  )

# Returns the user object.
exports.get_person = (client, person_id, callback) ->
  client.query('SELECT * FROM people WHERE person_id=' + person_id, (err, result) ->
    if err
      console.log(err)
      callback()
      return
    if (result.rowCount != 1)
      callback()
      return
    callback(result.rows[0])
  )

# Returns the user object.
exports.get_user_by_name = (client, username, callback) ->
  sql = "SELECT * FROM people WHERE username='" + username + "'"
  q = client.query(sql)
  q.on('error', (err) ->
    console.log(err)
  )
  q.on('row', (row) ->
    callback(row)
  )

exports.move_to = (client, person_id, room_id, callback) ->
  sql = "UPDATE people SET room_id=" + room_id + " WHERE person_id=" + person_id
  client.query(sql, (err, result) ->
    if err
      console.log(err)
      return callback(false)
    return callback(true)
  )
