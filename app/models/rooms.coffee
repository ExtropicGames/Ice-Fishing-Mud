exports.get_room = (client, room_id, callback) ->
  q = client.query("SELECT * FROM rooms WHERE room_id=" + room_id)
  q.on('error', (err) ->
    console.log(err)
  )
  q.on('row', (row) ->
    callback(row)
  )

# Gets the room a user is currently in.
exports.get_current_room = (client, person_id, callback) ->
  q = client.query("SELECT r.* FROM people p LEFT JOIN rooms r ON p.room_id = r.room_id WHERE person_id=" + person_id)
  q.on('error', (err) ->
    console.log(err)
  )
  q.on('row', (row) ->
    callback(row)
  )

exports.get_link = (client, room_id, direction, callback) ->
  dir = {
    'n': 1,
    's': 2,
    'e': 3,
    'w': 4,
    'u': 5,
    'd': 6,
  }
  sql = "
    SELECT * FROM room_links rl
    LEFT JOIN rooms r ON rl.come_to=r.room_id
    WHERE
      rl.go_from=" + room_id + " AND
      rl.direction=" + dir[direction] + "
  "
  client.query(sql, (err, result) ->

    if (err)
      console.log(err)
      return callback()
    # if there are multiple possible room links, they are chosen from non-deterministically
    return callback(result.rows[Math.floor(Math.random()*result.rows.length)])
  )

exports.get_all = (client, callback) ->
  q = client.query('SELECT * FROM rooms')
  rows = []
  q.on('error', (err) ->
    console.log(err)
  )
  q.on('row', (row) ->
    rows.push(row)
  )
  q.on('end', () ->
    callback(rows)
  )
