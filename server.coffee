# "Support for a --coffee option to the lcm command will be added to an upcoming release."
#   -- Jared Hanson, Jan 24 2013 http://stackoverflow.com/questions/14489677/how-do-i-set-coffescript-in-locomotivejs
locomotive = require('locomotive')

locomotive.boot('.', 'development', {"coffeeScript": true}, (err, server) -> 
  throw err if (err)

  server.listen(process.env.PORT or 5000, '0.0.0.0', ()-> 
    addr = this.address()
    console.log('listening on %s:%d', addr.address, addr.port);
  )
)