#!/usr/bin/env node

var sys = require('sys')
var exec = require('child_process').exec
var fs = require('fs')

function echo (error, stdout, stderr) {
  if (stdout) {
    sys.puts(stdout)
  }
  if (error) {
    sys.puts(error)
  }
  if (stderr) {
    sys.puts(stderr)
  }
}

// used to generate the password
function generate () {
  var chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghiklmnopqrstuvwxyz'
  var string = ''

  while (string.length < 32) {
    var randomNumber = Math.floor(Math.random() * chars.length)
    string += chars.substring(randomNumber, randomNumber + 1)
  }

  return string
}

var password = generate()

fs.writeFile('.env', 'DATABASE_URL=postgres://ice_fishing_mud:' + password + '@localhost:5432/ice_fishing_mud', function (err) {
  if (err) {
    console.log('Could not write to .env file. Please check filesystem permissions.')
    process.exit(1)
  } else {
    console.log('The randomly generated password is ' + password + '. This has been saved to your local .env file.')
  }
})

exec("echo CREATE USER ice_fishing_mud WITH PASSWORD \\'" + password + "\\' | psql -d template1", echo)
exec("echo 'CREATE DATABASE ice_fishing_mud; GRANT ALL PRIVILEGES ON DATABASE ice_fishing_mud TO ice_fishing_mud' | psql -d template1", echo)
